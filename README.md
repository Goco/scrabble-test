# React Scrabble Test

This project is created as a Frontend Application for the [react-tech-test](https://github.com/elderstudios/react-tech-test) of the Elder Studios.

## Getting started
### Backend
- Download the [react-tech-test](https://github.com/elderstudios/react-tech-test) repo.
- Install Docker and Docker Compose: https://docs.docker.com/compose
- Start the mock api by typing `docker-compose up` from the **react-tech-test's** root directory in your terminal.
- Confirm that the API documentation loads at: http://localhost:3000
- Confirm that the list of users loads at http://localhost:3000/users

### Frontend
- Install Node.js: https://nodejs.org/en/
- Install yarn: https://classic.yarnpkg.com/en/docs/install
- Download this repo.
- Initially you have to install all the 3rd party libraries by typing `yarn` from this projects root directory in your terminal.
- Start the frontend by typing `yarn start` from this projects root directory in your terminal.
- Confirm that the Frontend application is running at: http://localhost:3001 \
(NOT the port 3000, because that is the port where the mock api is running).


## Used 3rd party libraries
### Create React App
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app) using the TypeScript option. Every basic libraries needed for the react application are included

### Code formating
Application was developed using Visual Studio Code. 
Extensions used:
- [VS Code ESLint extension](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
- [Prettier - Code formatter](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)

Libraries added to devDependencies:
```
"eslint-plugin-prettier": "^3.2.0",
"prettier": "^2.2.1"
```
### Localization
For the localization it was used [react-intl](https://formatjs.io/docs/getting-started/installation/)

### Components
For basic compoments it was used [material-ui](https://material-ui.com/)

## Functionality
Structure of the Application's source:
```
react-crabble-test
├── README.md
└── src
    ├── assets
    ├── components
    ├── data
    ├── i18n
    ├── modules
        ├── game.tsx
        ├── leader-board.tsx
        ├── user.tsx
        └── user-detail.tsx
    ├── utils
    ├── app.tsx
    ├── common-types.tsx
    ├── index.css
    ├── index.tsx
    └── routes.tsx
```
**assets** - folder with images\
**components** - general components\
**data** - data context where all the backend data are stored\
**i18n** - localization context\
**modules** - components used specifically for part of the application\

## Missing Features (TODOs)
- better handling of exceptions from fetch (using some sort of notifications)
- adding tests for the whole application
