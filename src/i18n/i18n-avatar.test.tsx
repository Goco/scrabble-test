import { mount } from 'enzyme';

import { I18nCtxProvider } from './i18n-ctx-provider';
import { I18nAvatar } from './i18n-avatar';

describe('<AppBar /> test with default localisation', () => {
  const container = mount(
    <I18nCtxProvider>
      <I18nAvatar />
    </I18nCtxProvider>
  );

  it('should match the snapshot', () => {
    expect(container.html()).toMatchSnapshot();
  });

  it('should have a default localization', () => {
    const flag = container.find('img');
    expect(flag.length).toEqual(1);
    expect(flag.prop('alt')).toEqual('en');
  });

  it('should trigger change of localization', () => {
    const button = container.find('button');
    expect(button.length).toEqual(1);
    button.simulate('click');

    const flag = container.find('img');
    expect(flag.length).toEqual(1);
    expect(flag.prop('alt')).toEqual('cs');
  });

  it('should trigger change of localization back to default', () => {
    const button = container.find('button');
    expect(button.length).toEqual(1);
    button.simulate('click');

    const flag = container.find('img');
    expect(flag.length).toEqual(1);
    expect(flag.prop('alt')).toEqual('en');
  });
});
