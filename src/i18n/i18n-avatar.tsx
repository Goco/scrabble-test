import { useContext } from 'react';
import MuiAvatar from '@material-ui/core/Avatar';
import MuiIconButton from '@material-ui/core/IconButton';

import { I18nCtx } from './i18n-ctx';
import cs from 'assets/images/cs.jpg';
import en from 'assets/images/en.jpg';

const flags: Record<string, string> = { cs, en };

export function I18nAvatar() {
  const { locale, handleLocale } = useContext(I18nCtx);

  return (
    <MuiIconButton onClick={() => handleLocale(locale === 'cs' ? 'en' : 'cs')}>
      <MuiAvatar alt={locale} src={flags[locale]} />
    </MuiIconButton>
  );
}
