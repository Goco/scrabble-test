import {
  createContext,
  useCallback,
  useEffect,
  useMemo,
  useState,
} from 'react';

import en from './locale/lang-en.json';
import cs from './locale/lang-cs.json';

const i18nMap: Record<string, Record<string, string>> = {
  en,
  cs,
};

export type I18nCtxType = {
  locale: string;
  messages: Record<string, string>;
  handleLocale: (e: string) => void;
};

const defaultI18nCtx: I18nCtxType = {
  locale: 'en',
  messages: {},
  handleLocale: () => {},
};

export function useI18nCtx() {
  const [locale, setLocale] = useState(localStorage.getItem('lang') || 'en');

  const [messages, setMessages] = useState<Record<string, string>>(
    i18nMap[locale]
  );

  const handleLocale = useCallback((locale: string) => {
    setLocale(locale);
    setMessages(i18nMap[locale]);
  }, []);

  useEffect(() => {
    localStorage.setItem('lang', locale);
  }, [locale]);

  const localeContext: I18nCtxType = useMemo(() => {
    return {
      locale,
      messages,
      handleLocale,
    };
  }, [handleLocale, locale, messages]);

  return localeContext;
}

export const I18nCtx = createContext<I18nCtxType>(defaultI18nCtx);
