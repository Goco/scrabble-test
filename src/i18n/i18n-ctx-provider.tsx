import React from 'react';
import { IntlProvider } from 'react-intl';

import { I18nCtx, useI18nCtx } from './i18n-ctx';

export function I18nCtxProvider({ children }: { children: React.ReactNode }) {
  const i18nCtx = useI18nCtx();
  const { locale, messages } = i18nCtx;

  return (
    <I18nCtx.Provider value={i18nCtx}>
      <IntlProvider locale={locale} messages={messages}>
        {children}
      </IntlProvider>
    </I18nCtx.Provider>
  );
}
