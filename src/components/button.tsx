import MuiButton from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';

import { NOOP } from 'utils/noop';

type ButtonProps = {
  label: string;
  color?: 'inherit' | 'primary' | 'secondary' | 'default';
  handleClick?: () => void;
};

const useStyles = makeStyles((theme) => ({
  button: {
    color: 'white',
    marginRight: '1rem',
  },
}));

export const Button = ({
  label,
  color = 'inherit',
  handleClick = NOOP,
}: ButtonProps) => {
  const classes = useStyles();

  return (
    <MuiButton
      className={classes.button}
      variant={'contained'}
      color={color}
      onClick={handleClick}
    >
      {label}
    </MuiButton>
  );
};
