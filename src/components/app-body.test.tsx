import { mount } from 'enzyme';

import { AppBody } from './app-body';

describe('<AppBody /> test', () => {
  const container = mount(
    <AppBody>
      <div id="test">TEST</div>
    </AppBody>
  );

  it('should match the snapshot', () => {
    expect(container.html()).toMatchSnapshot();
  });
});
