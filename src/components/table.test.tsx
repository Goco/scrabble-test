import { BrowserRouter as Router } from 'react-router-dom';
import { mount } from 'enzyme';

import MuiSelect from '@material-ui/core/Select';

import { I18nCtxProvider } from 'i18n/i18n-ctx-provider';
import { Column, Table, TableProps } from './table';

type TestType = {
  id: number;
  testLabel: string;
  testValue: string;
};

const testColumns: Column<TestType>[] = [
  { label: 'testLabel' },
  { label: 'testValue' },
];

const testData: TestType[] = [...Array(50)].map((_, index) => ({
  id: index,
  testLabel: `label-${index}`,
  testValue: `value-${index}`,
}));

const setPageMock = jest.fn();
const setLimitMock = jest.fn();
const setSortMock = jest.fn();
const setOrderMock = jest.fn();
const onRowClickMock = jest.fn();

const testTableProps: TableProps<TestType> = {
  prefix: 'test',
  sort: 'testLabel',
  columns: testColumns,
  data: testData,
  setPage: setPageMock,
  setLimit: setLimitMock,
  setSort: setSortMock,
  setOrder: setOrderMock,
  onRowClick: onRowClickMock,
};

describe('<Table /> test', () => {
  const container = mount(
    <Router>
      <I18nCtxProvider>
        <Table {...testTableProps} />
      </I18nCtxProvider>
    </Router>
  );

  it('should match the snapshot', () => {
    expect(
      container
        .html()
        .replace(/id="mui-[0-9]*"/g, '')
        .replace(/aria-labelledby="(mui-[0-9]* *)*"/g, '')
    ).toMatchSnapshot();
  });

  it('should have a previous page button', () => {
    expect(container.find('button[title="Previous page"]').length).toEqual(1);
  });
  it('should have a  next page button', () => {
    expect(container.find('button[title="Next page"]').length).toEqual(1);
  });
  it('should trigger a next page button', () => {
    container.find('button[title="Next page"]').simulate('click');
    expect(setPageMock).toHaveBeenCalledTimes(1);
    expect(setPageMock).toHaveBeenCalledWith(1);
  });
  it('should trigger a change in rows per page', () => {
    const select = container.find(MuiSelect);
    expect(select.length).toEqual(1);

    // Not working, probably items are mounted outside of the tree
    // select.simulate('change', { target: { value: '25' } });
    // expect(setPageMock).toHaveBeenCalledTimes(1);
    // expect(setPageMock).toHaveBeenCalledWith(0);
    // expect(setLimitMock).toHaveBeenCalledTimes(1);
    // expect(setLimitMock).toHaveBeenCalledWith(25);
  });
  it('should trigger a filter', () => {
    const tableHeaders = container.find('th');
    expect(tableHeaders.length).toEqual(2);

    tableHeaders.at(0).simulate('click');
    expect(setOrderMock).toHaveBeenCalledTimes(1);
    expect(setOrderMock).toHaveBeenCalledWith('desc');

    tableHeaders.at(1).simulate('click');
    expect(setSortMock).toHaveBeenCalledTimes(1);
    expect(setSortMock).toHaveBeenCalledWith('testValue');

    expect(setOrderMock).toHaveBeenCalledTimes(2);
    expect(setOrderMock).toHaveBeenCalledWith('asc');
  });
  it('should trigger a second row click', () => {
    // second row is a first row in a table body (first is header)
    const tableRows = container.find('tr');
    expect(tableRows.length).toEqual(11);

    tableRows.at(1).simulate('click');
    expect(onRowClickMock).toHaveBeenCalledTimes(1);
  });
});
