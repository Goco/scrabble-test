import React from 'react';
import { Link } from 'react-router-dom';
import { useIntl } from 'react-intl';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import MuiContainer from '@material-ui/core/Container';
import MuiGrid from '@material-ui/core/Grid';
import MuiAppBar from '@material-ui/core/AppBar';
import MuiToolbar from '@material-ui/core/Toolbar';

import { routes } from 'routes';
import { I18nAvatar } from 'i18n/i18n-avatar';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100vw',
      height: '5rem',
      padding: '.5rem 1rem',
      backgroundColor: 'white',
      borderBottom: `5px solid ${theme.palette.primary.main}`,
    },
    link: {
      textDecoration: 'none',
      textTransform: 'uppercase',
      color: theme.palette.secondary.main,
    },
  })
);

export function AppBar() {
  const classes = useStyles();
  const { formatMessage } = useIntl();

  return (
    <MuiAppBar className={classes.root}>
      <MuiContainer maxWidth="lg">
        <MuiToolbar>
          <MuiGrid container spacing={4}>
            {routes
              .filter(({ label }) => !!label)
              .map(({ path, label }) => (
                <MuiGrid item key={path}>
                  <Link className={classes.link} to={path}>
                    {formatMessage({ id: `appbar.${label}` })}
                  </Link>
                </MuiGrid>
              ))}
          </MuiGrid>
          <I18nAvatar />
        </MuiToolbar>
      </MuiContainer>
    </MuiAppBar>
  );
}
