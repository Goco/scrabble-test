import React from 'react';
import { useIntl } from 'react-intl';
import { makeStyles } from '@material-ui/core/styles';
import MuiContainer from '@material-ui/core/Container';
import MuiPaper from '@material-ui/core/Paper';
import MuiTable from '@material-ui/core/Table';
import MuiTableBody from '@material-ui/core/TableBody';
import MuiTableCell from '@material-ui/core/TableCell';
import MuiTableContainer from '@material-ui/core/TableContainer';
import MuiTableHead from '@material-ui/core/TableHead';
import MuiTablePagination from '@material-ui/core/TablePagination';
import MuiTableRow from '@material-ui/core/TableRow';
import MuiTableSortLabel from '@material-ui/core/TableSortLabel';
import MuiCircularProgress from '@material-ui/core/CircularProgress';

import { NOOP } from 'utils/noop';
import { DataHookResponse } from 'data/data-hook';

export type Column<T> = {
  label: string;
  disableSort?: boolean;
  minWidth?: number;
  format?: (value: T, locale: string) => string;
};

export type TableProps<T> = DataHookResponse<T> & {
  prefix: string;
  columns?: Column<T>[];
  withoutPagination?: boolean;
  onRowClick?: (row: T) => void;
};

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: '2rem',
    width: '100%',
    height: 'calc(100% - 4rem)',
  },
  loaderWrapper: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    opacity: 1,
    zIndex: 1000,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    maxHeight: 500,
  },
  headerCell: {
    textTransform: 'uppercase',
  },
  headerCellSort: {
    textTransform: 'uppercase',
    cursor: 'pointer',
    color: theme.palette.secondary.main,
  },
  row: {},
  cell: {
    color: `${theme.palette.secondary.main} !important`,
  },
  clickableRow: {
    cursor: 'pointer',
  },
}));

export function Table<T extends { id: number }>(props: TableProps<T>) {
  const classes = useStyles();
  const { formatMessage, locale } = useIntl();

  const {
    prefix,
    data = [],
    columns = [],
    page = 0,
    setPage = NOOP,
    limit = 10,
    setLimit = NOOP,
    sort,
    setSort = NOOP,
    order,
    setOrder = NOOP,
    withoutPagination = false,
    onRowClick,
    loading,
  } = props;

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setLimit(+event.target.value);
    setPage(0);
  };

  const handleSort = (sortKey: string) => (_event: unknown) => {
    if (sort === sortKey) {
      setOrder(order === 'desc' ? 'asc' : 'desc');
    } else {
      setSort(sortKey);
      setOrder('asc');
    }
    setPage(0);
  };

  const dataArray = Object.values(data);

  return (
    <MuiContainer maxWidth="lg">
      {loading && (
        <div className={classes.loaderWrapper}>
          <MuiCircularProgress
            color="primary"
            style={{ width: 80, height: 80 }}
          />
        </div>
      )}
      <MuiPaper className={classes.root}>
        <MuiTableContainer className={classes.container}>
          <MuiTable stickyHeader aria-label="sticky table">
            <MuiTableHead>
              <MuiTableRow>
                {columns.map((column) => (
                  <MuiTableCell
                    key={column.label}
                    className={classes.cell}
                    classes={{
                      head: column.disableSort
                        ? classes.headerCell
                        : classes.headerCellSort,
                    }}
                    style={{ minWidth: column.minWidth }}
                    sortDirection={sort === column.label ? order : false}
                    onClick={
                      column.disableSort ? NOOP : handleSort(column.label)
                    }
                  >
                    {column.disableSort ? (
                      formatMessage({
                        id: `${prefix}.table.header.${column.label}`,
                      })
                    ) : (
                      <MuiTableSortLabel
                        classes={{ active: classes.cell }}
                        active={sort === column.label}
                        direction={sort === column.label ? order : 'asc'}
                      >
                        {formatMessage({
                          id: `${prefix}.table.header.${column.label}`,
                        })}
                      </MuiTableSortLabel>
                    )}
                  </MuiTableCell>
                ))}
              </MuiTableRow>
            </MuiTableHead>
            <MuiTableBody>
              {dataArray
                .slice(page * limit, page * limit + limit)

                .map((row) => {
                  return (
                    <MuiTableRow
                      hover
                      tabIndex={-1}
                      key={row.id}
                      onClick={() => onRowClick?.(row)}
                      className={
                        onRowClick ? classes.clickableRow : classes.row
                      }
                    >
                      {columns.map((column) => {
                        const format =
                          column.format ??
                          ((value: any) => value[column.label]);

                        return (
                          <MuiTableCell
                            className={classes.cell}
                            key={column.label}
                          >
                            {format(row, locale)}
                          </MuiTableCell>
                        );
                      })}
                    </MuiTableRow>
                  );
                })}
            </MuiTableBody>
          </MuiTable>
        </MuiTableContainer>
        {!withoutPagination && (
          <MuiTablePagination
            rowsPerPageOptions={[5, 10, 25, 100]}
            component="div"
            count={dataArray.length}
            rowsPerPage={limit}
            page={page}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}
          />
        )}
      </MuiPaper>
    </MuiContainer>
  );
}
