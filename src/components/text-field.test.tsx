import { mount } from 'enzyme';
import { TextField } from './text-field';

describe('<TextField /> test', () => {
  const setValueMock = jest.fn();
  const container = mount(
    <TextField label="test-label" value="test-value" setValue={setValueMock} />
  );
  const disabledContainer = mount(
    <TextField
      label="test-label"
      value={undefined}
      setValue={setValueMock}
      disabled={true}
    />
  );

  it('should match the snapshot', () => {
    expect(container.html()).toMatchSnapshot();
  });

  it('should match the disabled snapshot', () => {
    expect(disabledContainer.html()).toMatchSnapshot();
  });

  it('should have a label', () => {
    expect(container.find('label').length).toEqual(1);
  });

  it('should have an input', () => {
    expect(container.find('input').length).toEqual(1);
  });

  it('should trigger the setValue inside handleChange', () => {
    container
      .find('input')
      .simulate('change', { target: { value: 'new-value' } });
    expect(setValueMock).toHaveBeenCalledTimes(1);
    expect(setValueMock).toHaveBeenCalledWith('new-value');
  });
});
