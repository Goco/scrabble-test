import { makeStyles, Theme } from '@material-ui/core';

type TextFieldProps = {
  label: string;
  value: string | number | undefined;
  setValue?: (value: string) => void;
  disabled?: boolean;
};
const useStyles = makeStyles<Theme, { disabled?: boolean }>((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
  },
  label: {
    width: '100%',
    color: theme.palette.secondary.main,
  },
  input: {
    '&:focus': { outline: 'none' },
    borderRadius: '.5rem',
    padding: '.5rem',
    marginBottom: '1rem',
    border: ({ disabled }) =>
      `2px solid ${theme.palette.primary.main}${disabled ? '32' : 'ff'}`,
  },
}));

export const TextField = (props: TextFieldProps) => {
  const { label, value = '', disabled, setValue } = props;
  const classes = useStyles({ disabled });

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValue?.(event.target.value);
  };

  return (
    <div className={classes.root}>
      <label className={classes.label} htmlFor={label}>
        {label}
      </label>
      <input
        className={classes.input}
        id={label}
        value={value}
        disabled={disabled}
        onChange={handleChange}
      />
    </div>
  );
};
