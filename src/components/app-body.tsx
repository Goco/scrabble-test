import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100vw',
    height: 'calc(100vh - 5rem)',
    overflow: 'auto',
    backgroundColor: `${theme.palette.secondary.main}32`,
    paddingTop: '5rem',
  },
}));

export function AppBody({ children }: { children: React.ReactNode }) {
  const classes = useStyles();

  return <div className={classes.root}>{children}</div>;
}
