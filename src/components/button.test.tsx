import { mount } from 'enzyme';

import { Button } from './button';

describe('<Button /> test', () => {
  const handleClickMock = jest.fn();

  const container = mount(
    <Button label="test-label" handleClick={handleClickMock} />
  );
  const containerNOOPHandler = mount(<Button label="test-noop" />);

  it('should match the snapshot', () => {
    expect(container.html()).toMatchSnapshot();
  });
  it('should match the snapshot (noop)', () => {
    expect(containerNOOPHandler.html()).toMatchSnapshot();
  });

  it('should have a button', () => {
    expect(container.find('button').length).toEqual(1);
  });

  it('should trigger the handleClick', () => {
    container.find('button').simulate('click');
    expect(handleClickMock).toHaveBeenCalledTimes(1);
  });
});
