import { BrowserRouter as Router } from 'react-router-dom';
import { mount } from 'enzyme';

import { I18nCtxProvider } from 'i18n/i18n-ctx-provider';
import { AppBar } from './app-bar';

describe('<AppBar /> test with default localisation', () => {
  const container = mount(
    <Router>
      <I18nCtxProvider>
        <AppBar />
      </I18nCtxProvider>
    </Router>
  );

  it('should match the snapshot', () => {
    expect(container.html()).toMatchSnapshot();
  });
});
