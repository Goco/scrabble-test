export type FetchParams = Record<string, string | number | undefined>;
export type FetchProps = RequestInit & {
  params?: FetchParams;
};

function serialize(params: FetchParams = {}) {
  return Object.keys(params)
    .filter((key) => !!params[key])
    .map(
      (key) => `_${encodeURIComponent(key)}=${encodeURIComponent(params[key]!)}`
    )
    .join('&');
}

export async function myFetch(
  url: string,
  { params, ...rest }: FetchProps = {}
) {
  const queryParams = serialize(params);
  const questionMark = !!queryParams ? '?' : '';

  return fetch(`${url}${questionMark}${queryParams}`, rest);
}

export default myFetch;
