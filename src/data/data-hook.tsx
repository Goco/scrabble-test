import { useCallback, useEffect, useState } from 'react';
import fetch from 'utils/fetch';

type OrderType = 'asc' | 'desc';
export type DataHookProps = {
  url: string;
  defaultSort: string;
  defaultOrder?: OrderType;
  defaultLimit?: number;
};
export type DataHookResponse<T> = {
  loading?: boolean;
  setLoading?: (value: boolean) => void;
  page?: number;
  setPage?: (value: number) => void;
  limit?: number;
  setLimit?: (value: number) => void;
  sort?: string;
  setSort?: (value: string) => void;
  order?: OrderType;
  setOrder?: (value: OrderType) => void;
  data?: T[];
  setData?: (value: T[]) => void;
  reloadData?: () => void;
};

export function useDataHook<T>(props: DataHookProps): DataHookResponse<T> {
  const { url, defaultSort, defaultOrder = 'asc', defaultLimit = 10 } = props;

  const [loading, setLoading] = useState(true);
  const [reload, setReload] = useState(false);
  const [page, setPage] = useState(0);
  const [limit, setLimit] = useState(defaultLimit);
  const [sort, setSort] = useState(defaultSort);
  const [order, setOrder] = useState(defaultOrder);
  const [data, setData] = useState<T[]>([]);

  const reloadData = useCallback(() => {
    setReload(true);
  }, []);

  useEffect(() => {
    const params = { sort, order };
    setLoading(true);
    setReload(false);

    const getData = async () => {
      try {
        const response = await fetch(url, { params });

        const json: T[] = await response.json();

        setData(json);
      } catch (e) {
        console.error(e);
      } finally {
        setLoading(false);
      }
    };

    getData();
  }, [order, sort, url, reload]);

  return {
    loading,
    setLoading,
    page,
    setPage,
    limit,
    setLimit,
    sort,
    setSort,
    order,
    setOrder,
    data,
    setData,
    reloadData,
  };
}
