import React from 'react';

import { DataCtx, useDataCtx } from './data-ctx';

export function DataCtxProvider({ children }: { children: React.ReactNode }) {
  const dataCtx = useDataCtx();

  return <DataCtx.Provider value={dataCtx}>{children}</DataCtx.Provider>;
}
