import { mount } from 'enzyme';
import fetchMock from 'fetch-mock';
import 'whatwg-fetch';

import { DataCtxProvider } from './data-ctx-provider';
import { DataCtx, DataCtxType } from './data-ctx';

const TestComponent = (_props: { value: DataCtxType }) => {
  return <div id="test" />;
};

describe('<DataCtxProvider /> test', () => {
  beforeAll(() => {
    global.fetch = fetch;
  });
  afterAll(() => {
    fetchMock.restore();
  });

  it('should return default data', async () => {
    const container = mount(
      <DataCtxProvider>
        <DataCtx.Consumer>
          {(value) => <TestComponent value={value} />}
        </DataCtx.Consumer>
      </DataCtxProvider>
    );

    const testComponent = container.find(TestComponent);

    expect(container.find(TestComponent).length).toEqual(1);

    const games = testComponent.prop('value').games;
    const leaderBoard = testComponent.prop('value').leaderBoard;
    const users = testComponent.prop('value').users;

    // Games
    expect(games?.data).toEqual([]);
    expect(games?.limit).toEqual(5);
    expect(games?.order).toEqual('asc');
    expect(games?.sort).toEqual('createdAt');

    // LeaderBoard
    expect(leaderBoard?.data).toEqual([]);
    expect(leaderBoard?.limit).toEqual(10);
    expect(leaderBoard?.order).toEqual('desc');
    expect(leaderBoard?.sort).toEqual('wins');

    // Games
    expect(users?.data).toEqual([]);
    expect(users?.limit).toEqual(10);
    expect(users?.order).toEqual('asc');
    expect(users?.sort).toEqual('name');
  });
});
