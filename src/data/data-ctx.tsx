import { createContext } from 'react';

import { LeaderBoard, User, Game } from 'common-types';

import { DataHookProps, DataHookResponse, useDataHook } from './data-hook';

const leaderBoardProps: DataHookProps = {
  url: '/leaderBoard',
  defaultSort: 'wins',
  defaultOrder: 'desc',
};
const usersProps: DataHookProps = {
  url: '/users',
  defaultSort: 'name',
};
const gamesProps: DataHookProps = {
  url: '/games',
  defaultSort: 'createdAt',
  defaultLimit: 5,
};

export type DataCtxType = {
  leaderBoard?: DataHookResponse<LeaderBoard>;
  users?: DataHookResponse<User>;
  games?: DataHookResponse<Game>;
};

const defaultDataCtx: DataCtxType = {};

export function useDataCtx() {
  const leaderBoard = useDataHook<LeaderBoard>(leaderBoardProps);
  const users = useDataHook<User>(usersProps);
  const games = useDataHook<Game>(gamesProps);

  return { leaderBoard, users, games };
}

export const DataCtx = createContext<DataCtxType>(defaultDataCtx);
