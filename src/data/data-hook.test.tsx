import { renderHook, act } from '@testing-library/react-hooks';
import fetchMock from 'fetch-mock';
import 'whatwg-fetch';

import { DataHookProps, useDataHook } from 'data/data-hook';

const original = console.error;

const succesFetchProps: DataHookProps = {
  url: 'test-url-success',
  defaultSort: 'created',
};
const failedfetchProps: DataHookProps = {
  url: 'test-url-fail',
  defaultSort: 'created',
};
const testEntity = { id: 'success' };

describe('useDataHook test', () => {
  beforeAll(() => {
    global.fetch = fetch;
    console.error = jest.fn();
  });
  afterAll(() => {
    console.error = original;
    fetchMock.restore();
  });

  fetchMock.mock('/test-url-success?_sort=created&_order=asc', [testEntity]);
  fetchMock.mock('/test-url-fail?_sort=created&_order=asc', 500);

  it('should return test data with a successful request', async () => {
    const { result } = renderHook(() => useDataHook(succesFetchProps));

    await act(async () => {
      result.current.reloadData?.();
    });

    expect(result.current.loading).toBe(false);
    expect(result.current.data).toEqual([testEntity]);
  });

  it('should return empty data with a failed request', async () => {
    const { result } = renderHook(() => useDataHook(failedfetchProps));

    await act(async () => {
      result.current.reloadData?.();
    });

    expect(result.current.loading).toBe(false);
    expect(result.current.data).toEqual([]);
  });
});
