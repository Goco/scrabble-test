import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch,
} from 'react-router-dom';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';

import { I18nCtxProvider } from 'i18n/i18n-ctx-provider';
import { DataCtxProvider } from 'data/data-ctx-provider';
import { routes } from 'routes';
import { AppBar } from 'components/app-bar';
import { AppBody } from 'components/app-body';

export const myTheme = createMuiTheme({
  typography: {
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      'Segoe UI',
      'Roboto',
      'Oxygen',
      'Ubuntu',
      'Cantarell',
      'Fira Sans',
      'Droid Sans',
      'Helvetica Neue',
      'sans-serif',
    ].join(', '),
  },
  palette: {
    primary: {
      main: '#a9c309',
    },
    secondary: {
      main: '#706f6f',
    },
  },
});

function App() {
  return (
    <Router>
      <ThemeProvider theme={myTheme}>
        <I18nCtxProvider>
          <DataCtxProvider>
            <AppBar />
            <AppBody>
              <Switch>
                {routes.map(({ path, Component }) => (
                  <Route key={path} exact path={path} component={Component} />
                ))}
                <Redirect to="/leader-board" />
              </Switch>
            </AppBody>
          </DataCtxProvider>
        </I18nCtxProvider>
      </ThemeProvider>
    </Router>
  );
}

export default App;
