import { Column } from 'components/table';

export type GameView = {
  id: number;
  winningScore: number;
  opponentName: string;
  opponentScore: number;
  createdAt: string;
};

export const columns: Column<GameView>[] = [
  { label: 'winningScore', disableSort: true },
  { label: 'opponentScore', disableSort: true },
  { label: 'opponentName', disableSort: true },
  {
    label: 'createdAt',
    format: ({ createdAt }, locale) =>
      new Date(createdAt).toLocaleString(locale),
  },
];
