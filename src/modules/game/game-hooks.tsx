import { useContext, useMemo } from 'react';

import { User } from 'common-types';
import { DataCtx } from 'data/data-ctx';
import { DataHookResponse } from 'data/data-hook';
import { GameView } from './game-columns';

export function useGameTable(userId: string): DataHookResponse<GameView> {
  const { games, users } = useContext(DataCtx);
  const { data: gamesData = [], setData, ...rest } = games ?? {};
  const { data: usersData = [] } = users ?? {};

  const data: GameView[] = useMemo(() => {
    if (gamesData.length === 0) {
      return [];
    }
    const indexedUsersData: Record<number, User> = {};
    usersData.map((user) => (indexedUsersData[user.id] = user));

    return gamesData
      .filter(({ winnerId }) => winnerId === +userId)
      .map(({ id, scores: [firstPlayer, secondPlayer], createdAt }) => {
        const [currentPlayer, opponentPlayer] =
          firstPlayer.memberId === +userId
            ? [firstPlayer, secondPlayer]
            : [secondPlayer, firstPlayer];

        return {
          id,
          winningScore: currentPlayer.score,
          opponentScore: opponentPlayer.score,
          opponentName: indexedUsersData?.[opponentPlayer.memberId]?.name ?? '',
          createdAt,
        };
      });
  }, [gamesData, userId, usersData]);

  return { ...rest, data };
}
