import { useEffect, useState } from 'react';
import { useIntl } from 'react-intl';
import { makeStyles } from '@material-ui/core/styles';
import MuiContainer from '@material-ui/core/Container';
import MuiPaper from '@material-ui/core/Paper';
import MuiGrid from '@material-ui/core/Grid';

import { TextField } from 'components/text-field';
import { Button } from 'components/button';

import { UserDetailView } from './user-detail-hooks';

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: '2rem',
    width: '100%',
    height: 'calc(100% - 4rem)',
  },
  row: {
    padding: '0 1rem',
  },
  cell: {
    width: '100%',
  },
}));

export const UserDetailForm = (props: UserDetailView) => {
  const classes = useStyles();
  const { formatMessage, locale } = useIntl();
  const {
    name,
    username,
    email,
    wins,
    losses,
    averageScore,
    highestScore,
    handleSave,
  } = props;
  const createdAt =
    highestScore?.createdAt &&
    new Date(highestScore?.createdAt).toLocaleString(locale);

  const [formData, setFormData] = useState({ name, username, email });
  const [editable, setEditable] = useState(false);

  const handleChange = (input: 'name' | 'username' | 'email') => (
    value: string
  ) => {
    setFormData((oldValue) => ({ ...oldValue, [input]: value }));
  };

  useEffect(() => {
    setFormData({ name, username, email });
  }, [email, name, username]);

  return (
    <MuiContainer maxWidth="lg">
      <MuiPaper className={classes.root}>
        <MuiGrid container spacing={2} className={classes.row}>
          <MuiGrid item md={4}>
            <TextField
              label={formatMessage({ id: 'userDetail.form.name' })}
              value={formData.name}
              setValue={handleChange('name')}
              disabled={!editable}
            />
          </MuiGrid>
          <MuiGrid item md={4}>
            <TextField
              label={formatMessage({ id: 'userDetail.form.username' })}
              value={formData.username}
              setValue={handleChange('username')}
              disabled={!editable}
            />
          </MuiGrid>
          <MuiGrid item md={4}>
            <TextField
              label={formatMessage({ id: 'userDetail.form.email' })}
              value={formData.email}
              setValue={handleChange('email')}
              disabled={!editable}
            />
          </MuiGrid>
          <MuiGrid item md={4}>
            <TextField
              label={formatMessage({ id: 'userDetail.form.wins' })}
              value={wins}
              disabled={true}
            />
          </MuiGrid>
          <MuiGrid item md={4}>
            <TextField
              label={formatMessage({ id: 'userDetail.form.losses' })}
              value={losses}
              disabled={true}
            />
          </MuiGrid>
          <MuiGrid item md={4}>
            <TextField
              label={formatMessage({ id: 'userDetail.form.averageScore' })}
              value={averageScore}
              disabled={true}
            />
          </MuiGrid>
          <MuiGrid item md={4}>
            <TextField
              label={formatMessage({ id: 'userDetail.form.highestScore' })}
              value={highestScore?.score}
              disabled={true}
            />
          </MuiGrid>
          <MuiGrid item md={4}>
            <TextField
              label={formatMessage({ id: 'userDetail.form.opponentName' })}
              value={highestScore?.opponentName}
              disabled={true}
            />
          </MuiGrid>
          <MuiGrid item md={4}>
            <TextField
              label={formatMessage({ id: 'userDetail.form.createdAt' })}
              value={createdAt}
              disabled={true}
            />
          </MuiGrid>
          <MuiGrid item md={4}>
            {editable ? (
              <>
                <Button
                  label={formatMessage({ id: 'userDetail.form.save' })}
                  color={'primary'}
                  handleClick={() => {
                    handleSave(formData);
                    setEditable(false);
                  }}
                />
                <Button
                  label={formatMessage({ id: 'userDetail.form.cancel' })}
                  color={'secondary'}
                  handleClick={() => {
                    setFormData({ name, username, email });
                    setEditable(false);
                  }}
                />
              </>
            ) : (
              <Button
                label={formatMessage({ id: 'userDetail.form.edit' })}
                color={'primary'}
                handleClick={() => setEditable(true)}
              />
            )}
          </MuiGrid>
        </MuiGrid>
      </MuiPaper>
    </MuiContainer>
  );
};
