import { useCallback, useContext, useMemo, useState } from 'react';

import fetch from 'utils/fetch';
import { DataCtx } from 'data/data-ctx';

export type UserDetailFormData = {
  name?: string;
  username?: string;
  email?: string;
};
export type UserDetailView = {
  id?: number;
  name?: string;
  username?: string;
  email?: string;
  wins?: number;
  losses?: number;
  averageScore?: number;
  highestScore?: {
    score?: number;
    opponentName?: string;
    createdAt?: string;
  };

  loading: boolean;
  handleSave: (props: UserDetailFormData) => Promise<void>;
};

export function useUserDetail(userId: string): UserDetailView {
  const { leaderBoard, games, users } = useContext(DataCtx);
  const [loading, setLoading] = useState(false);

  const { data: leaderBoardData = [] } = leaderBoard ?? {};
  const { data: gamesData = [] } = games ?? {};
  const { data: usersData = [] } = users ?? {};

  const handleSave = useCallback(
    async (formData: UserDetailFormData) => {
      setLoading(true);
      try {
        const response = await fetch(`/users/${userId}`, {
          method: 'PATCH',
          headers: new Headers({
            'Content-Type': 'application/json',
          }),
          body: JSON.stringify(formData),
        });

        if (response.ok) {
          users?.reloadData?.();
        }
      } catch (e) {
        console.error(e);
      } finally {
        setLoading(false);
      }
    },
    [userId, users]
  );

  const userDetail = useMemo(() => {
    const leaderBoard = leaderBoardData.find(
      ({ userId: leaderBoardUserId }) => leaderBoardUserId === +userId
    );

    const displayedUser = usersData.find(({ id }) => id === +userId);
    const opponentUser = usersData.find(
      ({ id }) => id === leaderBoard?.highestScore?.against
    );

    const bestGame = gamesData.find(
      ({
        scores: [
          { memberId: firstId, score: firstScore },
          { memberId: secondId, score: secondScore },
        ],
      }) =>
        (firstId === displayedUser?.id &&
          secondId === leaderBoard?.highestScore?.against &&
          firstScore === leaderBoard?.highestScore?.score) ||
        (secondId === displayedUser?.id &&
          firstId === leaderBoard?.highestScore?.against &&
          secondScore === leaderBoard?.highestScore?.score)
    );

    const { id, name, username, email } = displayedUser ?? {};
    const { wins, losses, averageScore, highestScore } = leaderBoard ?? {};

    return {
      id,
      name,
      username,
      email,
      wins,
      losses,
      averageScore,
      highestScore: {
        score: highestScore?.score,
        opponentName: opponentUser?.name,
        createdAt: bestGame?.createdAt,
      },
    };
  }, [gamesData, leaderBoardData, userId, usersData]);

  return { ...userDetail, loading, handleSave };
}
