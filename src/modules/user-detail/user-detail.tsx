import { RouteComponentProps } from 'react-router-dom';

import { Table } from 'components/table';
import { useGameTable } from 'modules/game/game-hooks';
import { columns } from 'modules/game/game-columns';

import { useUserDetail } from './user-detail-hooks';
import { UserDetailForm } from './user-detail-form';

export const UserDetail = (props: RouteComponentProps<{ userId: string }>) => {
  const { userId } = props.match.params;

  const gameProps = useGameTable(userId);
  const userDetailProps = useUserDetail(userId);
  return (
    <>
      <UserDetailForm {...userDetailProps} />
      <Table
        prefix={'game'}
        columns={columns}
        {...gameProps}
        loading={gameProps.loading || userDetailProps.loading}
      />
      ;
    </>
  );
};
