import { useContext, useMemo } from 'react';

import { User } from 'common-types';
import { DataCtx } from 'data/data-ctx';
import { DataHookResponse } from 'data/data-hook';
import { LeaderBoardView } from './leader-board-columns';

export function useLeaderBoardTable(): DataHookResponse<LeaderBoardView> {
  const { leaderBoard, users } = useContext(DataCtx);
  const {
    data: leaderBoardData = [],
    loading: leaderBoardLoading,
    order,
    sort,
    setOrder,
    setSort,
  } = leaderBoard ?? {};
  const { data: usersData = [] } = users ?? {};

  const data: LeaderBoardView[] = useMemo(() => {
    if (leaderBoardData.length === 0) {
      return [];
    }
    const indexedUsersData: Record<number, User> = {};
    usersData.forEach((user) => (indexedUsersData[user.id] = user));

    return leaderBoardData.map(
      ({ averageScore, losses, userId, wins }, index) => ({
        place: index + 1,
        userName: indexedUsersData?.[userId]?.name ?? '',
        averageScore,
        losses,
        wins,
        id: userId,
      })
    );
  }, [leaderBoardData, usersData]);

  return { data, loading: leaderBoardLoading, order, sort, setOrder, setSort };
}
