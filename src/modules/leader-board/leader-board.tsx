import { Table } from 'components/table';
import { RouteComponentProps } from 'react-router-dom';

import { columns } from './leader-board-columns';
import { useLeaderBoardTable } from './leader-board-hooks';

export const LeaderBoardTable = (props: RouteComponentProps<{}>) => {
  const leaderBoardProps = useLeaderBoardTable();

  return (
    <Table
      prefix={'leaderBoard'}
      columns={columns}
      withoutPagination={true}
      {...leaderBoardProps}
    />
  );
};
