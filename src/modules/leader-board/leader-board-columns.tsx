import { Column } from 'components/table';

export type LeaderBoardView = {
  id: number;
  userName: string;
  wins: number;
  losses: number;
  averageScore: number;
};

export const columns: Column<LeaderBoardView>[] = [
  { label: 'place', disableSort: true },
  { label: 'userName', disableSort: true },
  { label: 'wins' },
  { label: 'losses' },
  { label: 'averageScore' },
];
