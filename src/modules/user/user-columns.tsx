import { User } from 'common-types';
import { Column } from 'components/table';

const formatDate = (key: 'createdAt' | 'updatedAt') => (
  user: User,
  locale: string
) => {
  const rawDate = user[key];
  return new Date(rawDate).toLocaleString(locale);
};

export const columns: Column<User>[] = [
  { label: 'name' },
  { label: 'username' },
  { label: 'email' },
  { label: 'updatedAt', format: formatDate('updatedAt') },
  { label: 'createdAt', format: formatDate('createdAt') },
];
