import { useContext } from 'react';
import { RouteComponentProps } from 'react-router-dom';

import { DataCtx } from 'data/data-ctx';
import { Table } from 'components/table';

import { columns } from './user-columns';

export const UserTable = (props: RouteComponentProps<{}>) => {
  const { history } = props;
  const { users } = useContext(DataCtx);

  return (
    <Table
      prefix={'user'}
      columns={columns}
      onRowClick={({ id }) => history.push(`users/${id}`)}
      {...users}
    />
  );
};
