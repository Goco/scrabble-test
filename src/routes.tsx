import { Route } from 'common-types';
import { LeaderBoardTable } from 'modules/leader-board/leader-board';
import { UserTable } from 'modules/user/user';
import { UserDetail } from 'modules/user-detail/user-detail';

export const routes: Route[] = [
  { path: '/leader-board', label: 'leaderBoard', Component: LeaderBoardTable },
  { path: '/users', label: 'users', Component: UserTable },
  { path: '/users/:userId', Component: UserDetail },
];
