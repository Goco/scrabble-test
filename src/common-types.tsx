import { RouteComponentProps } from 'react-router-dom';

export type Route = {
  path: string;
  Component: React.ComponentType<RouteComponentProps<any>>;
  label?: string;
};

export type User = {
  id: number;
  name: string;
  username: string;
  email: string;
  createdAt: string;
  updatedAt: string;
};

export type Game = {
  id: number;
  scores: [Score, Score];
  winnerId: number;
  createdAt: string;
};

export type Score = {
  memberId: number;
  score: number;
};

export type LeaderBoard = {
  userId: number;
  wins: number;
  losses: number;
  averageScore: number;
  highestScore: {
    score: number;
    against: number;
  };
};
